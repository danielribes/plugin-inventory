# README #

Este es un plugin para Wordpress que genera genera un listado fácil de copiar/pegar de los plugins instalados.

### Instalación ###

Clonar este repositorio dentro de /wp-contents/plugins  de manera que os quede: /wp-contents/plugins/plugin-inventory

En el admin de Wordpress activar el plugin. Una vez activo muestra la opción "Plugins Inventory" en el menú de administración.

En esta opción se listan los plugins instalados y los activos.

### Disclaimer ###

Este plugin es un trabajo en progreso, no me hago responsable de los errores o problemas derivados de su uso.