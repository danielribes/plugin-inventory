<?php
/*
 Plugin Name: Plugins inventory
 Plugin Uri:
 Description: Genera un listado fácil de copiar/pegar de los plugins instalados
 Version: 0.1
 Author: Daniel Ribes
 Author Uri: http://www.danielribes.com
 License: MIT License http://opensource.org/licenses/MIT
 
*/

//  Inicializa el item del menu admin WP
add_action('admin_menu', 'drpluginv_menu');
function drpluginv_menu()
{
    add_menu_page('Plugin Inventory','Plugin Inventory','manage_options','drpluginv','drpluginv_home','dashicons-admin-generic',110);
}


function drpluginv_home()
{
	echo '<h1>Plugin Inventory</h1>';
	echo '<p>Genera un listado fácil de copiar/pegar de los plugins instalados</p>';

	$los_plugins = get_plugins();

	echo '<h2>Todos los instalados:</h2>';	
	echo '<ul>';
	foreach ($los_plugins as $un_plugin)
	{
		echo '<li>'.$un_plugin['Name'];
		if(!empty($un_plugin['Version']))
		{
			echo ' - V: '.$un_plugin['Version'];
		}
		echo '<br /><em>'.$un_plugin['PluginURI'].'</em></li>';
	}
	echo "</ul>";

	echo '<h2>Solo los activados:</h2>';	
	echo '<ul>';
	foreach ($los_plugins as $k => $v)
	{
		if( is_plugin_active($k) )
		{
			echo '<li>'.$v['Name'];
			if(!empty($v['Version']))
			{
				echo ' - V: '.$v['Version'];
			}
			echo '<br /><em>'.$v['PluginURI'].'</em></li>';
		}
		
	}
	echo "</ul>";
}